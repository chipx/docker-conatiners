#!/bin/bash
BASEDIR=/var/cloud
if [[ $USER_ID != '' ]]; then
    SYSTEM_USER='cloud'
    groupadd -g ${USER_ID} cloud
    useradd -u ${USER_ID} -g ${USER_ID} -m -s /bin/bash cloud
fi

start-stop-daemon -b -o -c ${SYSTEM_USER} -S -u ${SYSTEM_USER} -x "/usr/bin/Xvfb" -- ":43" "-extension" "RANDR" "-extension" "GLX" "-screen" "0" "640x480x8" "-nolisten" "tcp"
sleep 1
export DISPLAY=":43"
CLOUD_COUNT=0
cat ${BASEDIR}/.accounts | while read line
do
        ((CLOUD_COUNT++))
	IFS=$'\t'
	set -- $line
	email=$1
	pass=$2
	folder=$3
	
	multi=""
	if [[ (($CLOUD_COUNT > 1)) ]]; then
            multi="-multi"
	fi
	
	logfile=""
        if [[ $LOG ]]; then
            logfile="$BASEDIR/$folder.log"
        fi
	
	echo -e "$CLOUD_COUNT:$email|$folder|$logfile"
        mkdir -p $BASEDIR/$folder
        chown cloud:cloud $BASEDIR/$folder
        chmod 775 $BASEDIR/$folder
        sleep 1
	start-stop-daemon -b -o -c ${SYSTEM_USER} -S -u ${SYSTEM_USER} -x "/usr/bin/cloud" -- "-email" "$email" "-password" "$pass" "-acceptLicense" "1" "-folder" "$BASEDIR/$folder" "-logfile" "$logfile" "$multi"
done
tail -f /dev/null