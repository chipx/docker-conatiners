#!/bin/bash
BASE_PATH=$(dirname $(readlink -f $0))
CONTAINER="mail_ru"
NAME="mail_ru"

echo "Stop running containers"
docker stop $(docker ps -f="name=${NAME}" -q)

echo "Remove old containers"
docker rm $(docker ps -a | awk '$2 ~ /'${CONTAINER}'/ && $NF ~ /'${NAME}'/{print $1}')

echo "Run container '${CONTAINER}' with name '${NAME}'"
CID=$(docker run -d -v /docker/mail_ru:/var/cloud -e USER_ID="1000" --name ${NAME} ${CONTAINER})
echo "Container ID -${CID}"

IP=$(docker inspect --format='{{.NetworkSettings.IPAddress}}' ${CID})
echo "IP - ${IP}"
