#!/bin/bash
BASE_PATH=$(dirname $(readlink -f $0))
CONTAINER="apollo:latest"
NAME="apollo"

if [ ! -e /etc/hosts-backup ]; then
    cp /etc/hosts /etc/hosts-backup
fi
echo "Stop running containers"
docker stop $(docker ps -f="name=${NAME}" -q)

echo "Remove old containers"
docker rm $(docker ps -a | awk '$2 ~ /'${CONTAINER}'/ && $NF ~ /'${NAME}'/{print $1}')

echo "Remove old from hosts"
sed -i "/#${CONTAINER}/d" /etc/hosts

echo "Run container '${CONTAINER}' with name '${NAME}'"
CID=$(docker run -d -p 61613 -p 61680:61680 -p 6181 \
	 -e TZ="Europe/Moscow" \
	--name ${NAME} \
	${CONTAINER} \
)
echo "Container ID -${CID}"

IP=$(docker inspect --format='{{.NetworkSettings.IPAddress}}' ${CID})
echo "IP - ${IP}"
echo "Add record to hosts"
echo "${IP} apollo.local #${CONTAINER}" >> /etc/hosts

docker exec -it $(docker ps -q -f='name=web-server') bash -c "echo '${IP} apollo.local' >> /etc/hosts"
