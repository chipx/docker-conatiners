#!/bin/bash
BASE_PATH=$(dirname $(readlink -f $0))
CONTAINER="web-server"
NAME="web-server"
if [ ! -e /etc/hosts-backup ]; then
    cp /etc/hosts /etc/hosts-backup
fi

EXISTS_CONTAINER=$(docker ps -f="name=${NAME}" -q)
if [ ! -z ${EXISTS_CONTAINER} ]; then
	echo "Stop running containers"
	docker stop ${EXISTS_CONTAINER}
fi

OLD_CONTAINER=$(docker ps -a | awk '$2 ~ /'${CONTAINER}'/ && $NF ~ /'${NAME}'/{print $1}')
if [ ! -z ${OLD_CONTAINER} ]; then
	echo "Remove old containers"
	docker rm ${OLD_CONTAINER}
fi

echo "Remove old from hosts"
sed -i "/#${CONTAINER}/d" /etc/hosts

echo "Run container '${CONTAINER}' with name '${NAME}'"
CID=$(docker run \
	 --name ${NAME} -d \
	-e XDEBUG_CONFIG="remote_host=172.17.0.1" \
	-e PHP_IDE_CONFIG="serverName=docker" \
	-e TERM="xterm-color" \
	-e TZ="Europe/Moscow" \
	-e HOSTNAME="web-server.local" \
	-p 80 -p 443 -p 9009 -p 24224 \
	-v ${BASE_PATH}/nginx-log:/var/log/nginx -v ${BASE_PATH}/var-www:/var/www -v ${BASE_PATH}/vhosts:/etc/nginx/vhosts \
	-v ${BASE_PATH}/crontabs:/var/spool/cron/crontabs \
	-v ${BASE_PATH}/home-remote:/home/remote \
	 ${CONTAINER})
echo "Container ID -${CID}"
IP=$(docker inspect --format='{{.NetworkSettings.IPAddress}}' ${CID})
echo "IP - ${IP}"
echo "Add record to hosts"
awk -v ip=${IP} -v container=${NAME} '$1 ~ /server_name/{print ip gensub(/server_name\W+([^;]+);/,"\\1",$0),"#" container}' ${BASE_PATH}/vhosts/*.conf | docker exec -i web-server bash -c "cat > /etc/hosts"
awk -v ip=${IP} -v container=${CONTAINER} '$1 ~ /server_name/{print ip gensub(/server_name\W+([^;]+);/,"\\1",$0),"#" container}' ${BASE_PATH}/vhosts/*.conf >> /etc/hosts
