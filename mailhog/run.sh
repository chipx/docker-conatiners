#!/bin/bash
BASE_PATH=$(dirname $(readlink -f $0))
CONTAINER="mailhog/mailhog"
NAME="mailhog"

if [ ! -e /etc/hosts-backup ]; then
    cp /etc/hosts /etc/hosts-backup
fi
echo "Stop running containers"
docker stop $(docker ps -f="name=${NAME}" -q)

echo "Remove old containers"
docker rm $(docker ps -a | awk '$NF ~ /'${NAME}'/{print $1}')

echo "Remove old from hosts"
sed -i "/#${NAME}/d" /etc/hosts

echo "Run container '${CONTAINER}' with name '${NAME}'"
CID=$(docker run -d \
	-p 1025 -p 8025 \
	 -e TZ="Europe/Moscow" \
	--name ${NAME} \
	${CONTAINER} \
)
echo "Container ID -${CID}"

IP=$(docker inspect --format='{{.NetworkSettings.IPAddress}}' ${CID})
echo "IP - ${IP}"
echo "Add record to hosts"
echo "${IP} mail.local #${CONTAINER}" >> /etc/hosts

docker exec -it $(docker ps -q -f='name=web-server') bash -c "echo '${IP} mail.local' >> /etc/hosts"
