#!/bin/bash
BASE_PATH=$(dirname $(readlink -f $0))
sudo ${BASE_PATH}/web-server/run.sh
sudo ${BASE_PATH}/mysql/run.sh
sudo ${BASE_PATH}/elasticsearch/run.sh

